# Introdução

## Equipes

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Eduardo Peroni|@eduardop02|
|Gizeli Simionatto|@gizelisimionat|

# Documentação

A documentação do projeto pode ser acessada pelo link:

>  https://eduardop02.gitlab.io/ie21cp20201/

# Links Úteis
(Projeto no tinkercad) https://www.tinkercad.com/things/29K5B5bbQLu-projeto-porta/editel


