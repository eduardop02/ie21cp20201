#include <Keypad.h> 
#include <Servo.h> 

Servo servo_Motor; 
char* senha = "123"; 
int position = 0; 
const byte linhas = 4; 
const byte colunas = 4; 
char keys[linhas][colunas] = { 
{'1','2','3','A'},
{'4','5','6','B'},
{'7','8','9','C'},
{'*','0','#','D'}
};

byte linhaPino[linhas] = { 11, 10, 9, 8 };
byte colunaPino[colunas] = { 7, 6, 5, 4 };  
Keypad keypad = Keypad( makeKeymap(keys), linhaPino, colunaPino, linhas, colunas );

const int ledVermelho = 1; 
const int ledVerde = 0; 

void setup(){
  pinMode(ledVermelho, OUTPUT); 
  pinMode(ledVerde, OUTPUT); 

  servo_Motor.attach(13); 
  setLocked(true); 
}

void loop(){

  
  char key = keypad.getKey(); 
  if (key == '*' || key == '#') { 
      position = 0; 
      setLocked(true); 
}
if (key == senha[position]){ 
      position ++;
}
if (position == 3){
      setLocked(false); 
}
delay(100);
}
void setLocked(int locked){ 
if (locked){ 
    digitalWrite(ledVermelho, HIGH);
    digitalWrite(ledVerde, LOW);
    servo_Motor.write(0); 
}
else{ 
    digitalWrite(ledVerde, HIGH);
    digitalWrite(ledVermelho, LOW);
    servo_Motor.write(90);
}
}